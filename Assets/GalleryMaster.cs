﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
//using UnityEditor;

public class GalleryMaster : MonoBehaviour {
    private static List<string> galleryImages;
    
    private static int num = 1;
    public GameObject galleryPrefab_inspector;
    private static GameObject galleryPrefab;
    private static GameObject gallery;
    public GameObject photoPannelPrefab_inspector;
    private static GameObject photoPannnelPrefab;
    private static GameObject photoPannel;

    private static Image[] photos;
    private static int size = 50;
    private static bool galleryToggle;
    private static Texture2D new_texture;
    private static byte[] fileData;

    void Awake()
    {
       galleryImages = GetAllGalleryImagePaths();
        galleryPrefab = galleryPrefab_inspector;
        photoPannnelPrefab = photoPannelPrefab_inspector;
    }

    public void GalleryButton()
    {
        if (!galleryToggle)
        {
            StartCoroutine(LoadGallery(galleryImages));
            
        }
        else
        {
            StartCoroutine(UnloadGallery());
            
        }
    }

    public static IEnumerator LoadGallery(List<string> galleryImages)
    {
        if(gallery == null) gallery = Instantiate(galleryPrefab, new Vector3(0,-10,0), Quaternion.identity);
        for (int i = 0; i < 10; i++)
        {
            gallery.transform.Translate(0, 1f, 0);
            yield return null;
        }
        photos = gallery.GetComponentsInChildren<Image>();
        for (int i = 0; i < photos.Length; i++)        
        {
            //WWW www = new WWW("file://" + galleryImages[galleryImages.Count - num++]);

            //yield return www;
            //new_texture = new Texture2D(16, 16);
            //www.LoadImageIntoTexture(new_texture);
            //Debug.Log(new_texture.width + "/a/" + new_texture.height);

            do
            {
               fileData = File.ReadAllBytes(galleryImages[galleryImages.Count - num++]);
                Debug.Log(fileData.Length);
                yield return null;
            } while (fileData.Length > 1000000);

            new_texture = new Texture2D(2, 2);

            new_texture.LoadImage(fileData); //..this will auto-resize the texture dimensions.


            //image.sprite = Sprite.Create(tex, new Rect(0, 0, 64, 64), new Vector2(0, 0));
            size = Mathf.Min(new_texture.width, new_texture.height);

            Debug.Log(new_texture.width + "/b/" + new_texture.height);

            photos[i].sprite = Sprite.Create(new_texture, new Rect(0, 0, size, size), new Vector2(0, 0));
            //photos[i].texture = new_texture;

            //fileData = null;
            //new_texture = null;
            System.GC.Collect();

            galleryToggle = !galleryToggle;
            yield return null;
        }
    }

    public static IEnumerator UnloadGallery()
    {
        for (int i = 0; i < 10; i++)
        {
            gallery.transform.Translate(0, -1f, 0);
            yield return null;
        }
        Destroy(gallery);
        System.GC.Collect();
        galleryToggle = !galleryToggle;
        yield return null;
    }

    public static IEnumerator Photo2World(int index)
    {
        yield return null;
        photoPannel = Instantiate(photoPannnelPrefab, Vector3.zero, Quaternion.identity);
        photoPannel.transform.Rotate(0f, 180f, 0f);
        fileData = File.ReadAllBytes(galleryImages[galleryImages.Count - (num -9 + index)]);       
        new_texture.LoadImage(fileData);
        photoPannel.transform.GetComponent<Renderer>().material.mainTexture = new_texture;
        yield return null;
    }



    private List<string> GetAllGalleryImagePaths()
    {
        List<string> results = new List<string>();
        HashSet<string> allowedExtesions = new HashSet<string>() { ".png", ".jpg", ".jpeg" };

        try
        {
            AndroidJavaClass mediaClass = new AndroidJavaClass("android.provider.MediaStore$Images$Media");

            // Set the tags for the data we want about each image.  This should really be done by calling; 
            //string dataTag = mediaClass.GetStatic<string>("DATA");
            // but I couldn't get that to work...

            const string dataTag = "_data";

            string[] projection = new string[] { dataTag };
            AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = player.GetStatic<AndroidJavaObject>("currentActivity");

            string[] urisToSearch = new string[] { "EXTERNAL_CONTENT_URI", "INTERNAL_CONTENT_URI" };
            foreach (string uriToSearch in urisToSearch)
            {
                AndroidJavaObject externalUri = mediaClass.GetStatic<AndroidJavaObject>(uriToSearch);
                Debug.Log("externalUri:" + externalUri);
                AndroidJavaObject finder = currentActivity.Call<AndroidJavaObject>("managedQuery", externalUri, projection, null, null, null);
                bool foundOne = finder.Call<bool>("moveToFirst");
                while (foundOne)
                {
                    int dataIndex = finder.Call<int>("getColumnIndex", dataTag);
                    string data = finder.Call<string>("getString", dataIndex);
                    if (allowedExtesions.Contains(Path.GetExtension(data).ToLower()))
                    {
                        string path = /*@"file:///" +*/ data;
                        Debug.Log("data:" + data);
                        results.Add(path);
                    }

                    foundOne = finder.Call<bool>("moveToNext");
                }
            }
        }
        catch (System.Exception e)
        {
            Debug.LogWarning(e);
            // do something with error...
        }

        return results;
    }

}
